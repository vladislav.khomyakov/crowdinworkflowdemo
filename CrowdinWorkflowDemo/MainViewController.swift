import UIKit
import SnapKit

final class MainViewController: UIViewController {

    private let label1 = UILabel()
    private let label2 = UILabel()
    private let label3 = UILabel()
    private let button = UIButton(type: .system)
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = L10n.Main.title
        label1.text = L10n.Main.Label1.text
        label2.text = L10n.Main.Label2.text
        label3.text = L10n.Main.Label3.text
        button.setTitle(L10n.Main.Button.title, for: .normal)
        
        let vStack = UIStackView(arrangedSubviews: [label1, label2, label3, button])
        vStack.axis = .vertical
        vStack.alignment = .center
        vStack.spacing = 16
        
        view.addSubview(vStack)
        
        vStack.snp.makeConstraints { make in
            make.leading.top.trailing.equalTo(view.safeAreaLayoutGuide)
        }
    }


}

